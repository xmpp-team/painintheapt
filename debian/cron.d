# cron entry for Pain in the APT
# just in case, you don't like to use systemd timers

#0 1 * * Mon-Sat root if test -x /usr/sbin/painintheapt; then /usr/sbin/painintheapt; else true; fi
#0 1 * * Sun     root if test -x /usr/sbin/painintheapt; then /usr/sbin/painintheapt --force; else true; fi
